# Perfect Pay | Challenge

## Instale as dependências do projeto
```
yarn install
```

### Suba a API fake para exibir os produtos
```
json-server db.json
```

### Compile o projeto e visualize no navegador
```
yarn serve
```